package com.example.weatherinfophotos;

import android.content.Context;

import com.example.weatherinfophotos.di.module.DatabaseModule;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DataBaseHelper {

    private final DatabaseModule databaseModule;
    private final Context mContext;

    @Inject
    public DataBaseHelper(DatabaseModule databaseModule, Context mContext) {
        this.databaseModule = databaseModule;
        this.mContext = mContext;
    }
}
