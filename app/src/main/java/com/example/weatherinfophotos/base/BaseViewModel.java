package com.example.weatherinfophotos.base;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.weatherinfophotos.data.AppDataManger;
import com.example.weatherinfophotos.data.Repository;
import com.example.weatherinfophotos.di.provides.SchedulerProvider;

import io.reactivex.disposables.CompositeDisposable;


public abstract class BaseViewModel extends ViewModel {

    private final MutableLiveData mIsLoading = new MutableLiveData<>();


    private final Repository repository;
    private CompositeDisposable mCompositeDisposable;
    private SchedulerProvider schedulerProvider;
    //   private WeakReference<N> mNavigator;
    private AppDataManger appDataManger;

    public BaseViewModel(
            Repository repository, SchedulerProvider schedulerProvider) {
        this.repository = repository;
        this.schedulerProvider = schedulerProvider;
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        mCompositeDisposable.dispose();
        super.onCleared();
    }

    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }


    public MutableLiveData<Boolean> getIsLoading() {
        return mIsLoading;
    }

    public void setIsLoading(boolean isLoading) {
        mIsLoading.setValue(isLoading);
    }

}
