package com.example.weatherinfophotos.data;


import androidx.lifecycle.LiveData;

import com.example.weatherinfophotos.data.db.entity.WeatherImages;
import com.example.weatherinfophotos.data.models.WeatherResponse;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import retrofit2.http.Query;

@Singleton
public class Repository {

    private AppDataManger appDataManger;
    private final String app_id = "5ff4405b2ce8d6d18dc8f4012aacb1e3";
    private final String units = "metric";

    @Inject
    public Repository(AppDataManger appDataManger) {
        this.appDataManger = appDataManger;
    }

    public Single<WeatherResponse> getWeather(@Query("lat") String lat,
                                              @Query("lon") String lon) {
        return appDataManger.getmApiHelper().getWeather(lat, lon, app_id, units);
    }

    public LiveData<List<WeatherImages>> getAllImages() {
        return appDataManger.weatherImagesDatabase.ImageDao().getAllImages();
    }

    public void insertImage(WeatherImages WeatherImages) {
        appDataManger.weatherImagesDatabase.ImageDao().insert(WeatherImages);
    }


}


