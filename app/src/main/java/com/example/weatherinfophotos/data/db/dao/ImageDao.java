package com.example.weatherinfophotos.data.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.weatherinfophotos.data.db.entity.WeatherImages;

import java.util.List;

@Dao
public interface ImageDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(WeatherImages weatherImages);

    @Query("SELECT * from images_table")
    LiveData<List<WeatherImages>> getAllImages();

}
