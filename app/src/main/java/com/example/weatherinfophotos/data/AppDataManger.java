package com.example.weatherinfophotos.data;

import android.content.Context;

import com.example.weatherinfophotos.data.db.WeatherImagesDatabase;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AppDataManger {
    private final ApiHelper mApiHelper;
    private final Context mContext;
    WeatherImagesDatabase weatherImagesDatabase;

    @Inject
    public AppDataManger(ApiHelper mApiHelper, Context mContext, WeatherImagesDatabase weatherImagesDatabase) {
        this.mApiHelper = mApiHelper;
        this.mContext = mContext;
        this.weatherImagesDatabase = weatherImagesDatabase;
    }


    public ApiHelper getmApiHelper() {
        return mApiHelper;
    }


    public Context getmContext() {
        return mContext;
    }

}
