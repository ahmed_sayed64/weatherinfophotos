package com.example.weatherinfophotos.data.db.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "images_table")

public class WeatherImages {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "image_path")
    private String mImage_path;

    public WeatherImages(@NonNull String mImage_path) {
        this.mImage_path = mImage_path;
    }

    @NonNull
    public String getImage_path() {
        return mImage_path;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
