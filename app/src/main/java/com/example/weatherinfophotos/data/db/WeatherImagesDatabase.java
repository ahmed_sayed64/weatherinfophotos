package com.example.weatherinfophotos.data.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.weatherinfophotos.data.db.dao.ImageDao;
import com.example.weatherinfophotos.data.db.entity.WeatherImages;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {WeatherImages.class}, version = 1, exportSchema = false)
public abstract class WeatherImagesDatabase extends RoomDatabase {

    public abstract ImageDao ImageDao();


}
