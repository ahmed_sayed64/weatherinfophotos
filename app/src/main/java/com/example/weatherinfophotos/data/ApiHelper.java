package com.example.weatherinfophotos.data;

import com.example.weatherinfophotos.data.models.WeatherResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiHelper {


    String BASE_URL = "http://api.openweathermap.org/data/2.5/";

    @GET("weather")
    Single<WeatherResponse> getWeather(@Query("lat") String lat,
                                       @Query("lon") String lon,
                                       @Query("appid") String app_id,
                                       @Query("units") String units);


}
