package com.example.weatherinfophotos.di.module;

import android.content.Context;
import android.util.Log;

import androidx.multidex.BuildConfig;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import timber.log.Timber;

@Module
public class NetworkModule {
    private static int REQUEST_TIMEOUT = 60;

    @Provides
    @Singleton
    public HttpLoggingInterceptor httpLoggingInterceptor() {
        return new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                if (BuildConfig.DEBUG) {
                    Timber.i(message);
                }
            }
        }).setLevel(HttpLoggingInterceptor.Level.BODY);
    }


    @Provides
    @Singleton
    public Cache cache(File fileCach) {
        return new Cache(fileCach, 10 * 1000 * 1000); // 10MB
    }

    @Provides
    @Singleton
    public File fileCach(Context context) {
        return new File(context.getCacheDir(), "okhttp_cach");
    }

    @Singleton
    public class LoggingInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
           // LogUtil.error("LoggingInterceptor", "inside intercept callback");
            Request request = chain.request();
            long t1 = System.nanoTime();
            String requestLog = String.format("Sending request %s on %s%n%s",
                    request.url(), chain.connection(), request.headers());
            if (request.method().compareToIgnoreCase("post") == 0) {
                requestLog = "\n" + requestLog + "\n" + bodyToString(request.body());
            }

            Response response = chain.proceed(request);
            long t2 = System.nanoTime();

            String responseLog = String.format("Received response for %s in %.1fms%n%s",
                    response.request().url(), (t2 - t1) / 1e6d, response.headers());

            String bodyString = response.body().string();
            return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), bodyString))
                    .build();
        }
    }

    @Provides
    @Singleton
    public Interceptor interceptor() {
        return new Interceptor() {
            @NotNull
            @Override
            public Response intercept(@NotNull Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Accept", "application/json,text/plain,*/*")
                        .addHeader("Content-Type", "application/json");

                Request request = requestBuilder.build();
                try {
                    String url = request.url().toString();
                    Response response = chain.proceed(request);

                    Log.e("urlHelper", url);
                    Log.e("bodyHelper", NetworkModule.this.bodyToString(request.body()) + "");
                    Log.e("headerHelper", request.headers().toString());
                    Log.e("ResponseHelper", response.body().string() + "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return chain.proceed(request);
            }
        };
    }




    @Provides
    @Singleton
    public OkHttpClient okHttpClient(HttpLoggingInterceptor httpLoggingInterceptor, Cache cache, Interceptor interceptor) {
        //   LoggingInterceptor interceptor1 = new LoggingInterceptor();
        return new OkHttpClient.Builder()
                .addInterceptor(new LoggingInterceptor())
                .addInterceptor(interceptor)
                .cache(cache)
                .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .build();
    }

    private String bodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            if (copy != null)
                copy.writeTo(buffer);
            else
                return "";
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }
}
