package com.example.weatherinfophotos.di.component;

import android.app.Application;

import com.example.weatherinfophotos.WeatherInfoPhotoApp;
import com.example.weatherinfophotos.di.builder.ActivityBuilderModule;
import com.example.weatherinfophotos.di.builder.FragmentBuilderModule;
import com.example.weatherinfophotos.di.module.AppModule;
import com.example.weatherinfophotos.di.module.DatabaseModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {AndroidInjectionModule.class, AppModule.class, ActivityBuilderModule.class, FragmentBuilderModule.class})
public interface AppComponent {

    void inject(WeatherInfoPhotoApp app);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}
