package com.example.weatherinfophotos.di.module;

import android.app.Application;
import android.content.Context;

import com.example.weatherinfophotos.data.ApiHelper;
import com.example.weatherinfophotos.di.provides.AppSchedulerProvider;
import com.example.weatherinfophotos.di.provides.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = {NetworkModule.class, DatabaseModule.class})
public class AppModule {

    @Singleton
    @Provides
    ApiHelper provideApiService(OkHttpClient okhttpClient) {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ApiHelper.BASE_URL)
                .client(okhttpClient)
                .build().create(ApiHelper.class);
    }

    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }


    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }


}
