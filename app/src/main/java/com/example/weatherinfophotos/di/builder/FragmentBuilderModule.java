package com.example.weatherinfophotos.di.builder;

import com.example.weatherinfophotos.ui.history.HistoryFragment;
import com.example.weatherinfophotos.ui.home.HomeFragment;
import com.example.weatherinfophotos.ui.splash.SplashFragment;
import com.example.weatherinfophotos.ui.weather.WeatherFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuilderModule {
    @ContributesAndroidInjector
    abstract SplashFragment contributeSplashFragment();
    @ContributesAndroidInjector
    abstract HomeFragment contributeHomeFragment();
    @ContributesAndroidInjector
    abstract WeatherFragment contributeWeatherFragment();
    @ContributesAndroidInjector
    abstract HistoryFragment contributeHistoryFragment();


}
