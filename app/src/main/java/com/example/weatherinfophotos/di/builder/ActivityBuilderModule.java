package com.example.weatherinfophotos.di.builder;

import com.example.weatherinfophotos.ui.main.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
@Module
public abstract class ActivityBuilderModule {

    @ContributesAndroidInjector
    abstract MainActivity contributeMainActivity();

}
