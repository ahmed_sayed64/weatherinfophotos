package com.example.weatherinfophotos.di.module;

import android.content.Context;

import androidx.room.Room;

import com.example.weatherinfophotos.data.db.WeatherImagesDatabase;
import com.example.weatherinfophotos.data.db.dao.ImageDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {

    @Singleton
    @Provides
    WeatherImagesDatabase getWeatherImagesDatabase(Context context) {
        return Room.databaseBuilder(context, WeatherImagesDatabase.class, "WeatherImages_database").build();
    }

    @Singleton
    @Provides
    ImageDao getImageDao(WeatherImagesDatabase db){
        return db.ImageDao();
    }
}
