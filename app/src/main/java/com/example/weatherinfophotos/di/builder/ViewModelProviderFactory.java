package com.example.weatherinfophotos.di.builder;


import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.weatherinfophotos.data.Repository;
import com.example.weatherinfophotos.di.provides.SchedulerProvider;
import com.example.weatherinfophotos.ui.history.HistoryViewModel;
import com.example.weatherinfophotos.ui.home.HomeViewModel;
import com.example.weatherinfophotos.ui.main.MainViewModel;
import com.example.weatherinfophotos.ui.splash.SplashViewModel;
import com.example.weatherinfophotos.ui.weather.WeatherViewModel;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ViewModelProviderFactory implements ViewModelProvider.Factory {


    private final Repository repository;
    private SchedulerProvider schedulerProvider;

    @Inject
    public ViewModelProviderFactory(Repository repository, SchedulerProvider schedulerProvider) {
        this.repository = repository;
        this.schedulerProvider = schedulerProvider;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(SplashViewModel.class)) {
            //noinspection unchecked
            return (T) new SplashViewModel(repository, schedulerProvider);
        } else if (modelClass.isAssignableFrom(MainViewModel.class)) {
            //noinspection unchecked
            return (T) new MainViewModel(repository, schedulerProvider);
        } else if (modelClass.isAssignableFrom(HomeViewModel.class)) {
            //noinspection unchecked
            return (T) new HomeViewModel(repository, schedulerProvider);
        } else if (modelClass.isAssignableFrom(WeatherViewModel.class)) {
            //noinspection unchecked
            return (T) new WeatherViewModel(repository, schedulerProvider);
        } else if (modelClass.isAssignableFrom(HistoryViewModel.class)) {
            //noinspection unchecked
            return (T) new HistoryViewModel(repository, schedulerProvider);
        }
        throw new IllegalArgumentException("Unknown ViewModel class: " + modelClass.getName());
    }
}