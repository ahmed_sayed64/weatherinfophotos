package com.example.weatherinfophotos.ui.history;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherinfophotos.R;
import com.example.weatherinfophotos.data.db.entity.WeatherImages;
import com.example.weatherinfophotos.databinding.ViewItemPhotoBinding;

import java.io.File;
import java.util.List;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ImagesViewHolder> {


    private List<WeatherImages> weatherImages;
    private Context context;

    public ImagesAdapter(Context context, List<WeatherImages> weatherImages) {
        this.context = context;
        this.weatherImages = weatherImages;
    }


    @NonNull
    @Override
    public ImagesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ImagesViewHolder(DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_item_photo, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ImagesViewHolder holder, int position) {

        WeatherImages weatherImage = weatherImages.get(position);

        File imgFile = new File(weatherImage.getImage_path());

        if (imgFile.exists()) {

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            holder.viewItemPhotoBinding.imageItem.setImageBitmap(myBitmap);

        }

    }

    @Override
    public int getItemCount() {
        return weatherImages.size();
    }


    public class ImagesViewHolder extends RecyclerView.ViewHolder {
        ViewItemPhotoBinding viewItemPhotoBinding;

        ImagesViewHolder(ViewItemPhotoBinding viewItemPhotoBinding) {
            super(viewItemPhotoBinding.getRoot());
            this.viewItemPhotoBinding = viewItemPhotoBinding;
        }
    }

    public interface ClickAction {
        void onImageClick(String image_path);
    }

}

