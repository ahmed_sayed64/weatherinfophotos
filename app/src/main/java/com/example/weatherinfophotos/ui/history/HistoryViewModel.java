package com.example.weatherinfophotos.ui.history;

import androidx.lifecycle.LiveData;

import com.example.weatherinfophotos.base.BaseViewModel;
import com.example.weatherinfophotos.data.Repository;
import com.example.weatherinfophotos.data.db.entity.WeatherImages;
import com.example.weatherinfophotos.di.provides.SchedulerProvider;

import java.util.List;

public class HistoryViewModel extends BaseViewModel {

    private LiveData<List<WeatherImages>> listImagesMutableLiveData;
    private final Repository repository;
    private final SchedulerProvider schedulerProvider;

    public HistoryViewModel(Repository repository, SchedulerProvider schedulerProvider) {
        super(repository, schedulerProvider);
        this.listImagesMutableLiveData = repository.getAllImages();
        this.repository = repository;
        this.schedulerProvider = schedulerProvider;

    }

    void insertImage(WeatherImages weatherImages) {
        repository.insertImage(weatherImages);
    }

    public LiveData<List<WeatherImages>> getListImagesMutableLiveData() {
        return listImagesMutableLiveData;
    }

}
