package com.example.weatherinfophotos.ui.main;

import android.os.Bundle;

import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.example.weatherinfophotos.R;
import com.example.weatherinfophotos.base.BaseActivity;
import com.example.weatherinfophotos.databinding.ActivityMainBinding;
import com.example.weatherinfophotos.di.builder.ViewModelProviderFactory;

import javax.inject.Inject;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> {

    @Inject
    ViewModelProviderFactory factory;
    NavController navController;

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public MainViewModel getViewModel() {
        return ViewModelProviders.of(this, factory).get(MainViewModel.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        navController = Navigation.findNavController(this, R.id.container_fragment);
        NavigationUI.setupWithNavController(getViewDataBinding().navView, navController);

    }


}
