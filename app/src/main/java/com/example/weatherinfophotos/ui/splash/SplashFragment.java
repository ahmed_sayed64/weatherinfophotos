package com.example.weatherinfophotos.ui.splash;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.weatherinfophotos.R;
import com.example.weatherinfophotos.base.BaseFragment;
import com.example.weatherinfophotos.databinding.FragmentSplashBinding;
import com.example.weatherinfophotos.di.builder.ViewModelProviderFactory;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import javax.inject.Inject;


public class SplashFragment extends BaseFragment<FragmentSplashBinding, SplashViewModel> {

    @Inject
    ViewModelProviderFactory factory;
    private Animation animation;
    private NavController navController;

    public SplashFragment() {
        // Required empty public constructor
    }


    @Override
    public int getLayoutId() {
        return R.layout.fragment_splash;
    }

    @Override
    public SplashViewModel getViewModel() {
        return ViewModelProviders.of(this, factory).get(SplashViewModel.class);

    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(getActivity(), R.id.container_fragment);
        hideToolbarAndBottomNav();
        fadeIn();

    }

    private void fadeIn() {
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                fadeOut();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        getViewDataBinding().rootView.startAnimation(animation);
    }

    private void fadeOut() {
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                goToHome();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        getViewDataBinding().rootView.startAnimation(animation);
    }

    private void goToHome() {
        int SPLASH_TIME_OUT = 1000;
        new Handler().postDelayed(() -> navController.navigate(R.id.action_splashFragment_to_homeFragment), SPLASH_TIME_OUT);
    }

    private void hideToolbarAndBottomNav() {
        if (getActivity() != null) {
            Toolbar mToolbar = getActivity().findViewById(R.id.toolbar);
            mToolbar.setVisibility(View.GONE);
            BottomNavigationView bottomNavigationView = getActivity().findViewById(R.id.nav_view);
            bottomNavigationView.setVisibility(View.GONE);
        }

    }

}
