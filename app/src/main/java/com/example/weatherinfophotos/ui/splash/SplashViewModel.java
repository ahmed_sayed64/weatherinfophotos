package com.example.weatherinfophotos.ui.splash;

import com.example.weatherinfophotos.base.BaseViewModel;
import com.example.weatherinfophotos.data.Repository;
import com.example.weatherinfophotos.di.provides.SchedulerProvider;

public class SplashViewModel extends BaseViewModel {

    public SplashViewModel(Repository repository, SchedulerProvider schedulerProvider) {
        super(repository, schedulerProvider);
    }
}
