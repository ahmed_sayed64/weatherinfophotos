package com.example.weatherinfophotos.ui.home;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.weatherinfophotos.R;
import com.example.weatherinfophotos.base.BaseFragment;
import com.example.weatherinfophotos.databinding.HomeFragmentBinding;
import com.example.weatherinfophotos.di.builder.ViewModelProviderFactory;
import com.example.weatherinfophotos.util.LocationHelper;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.File;

import javax.inject.Inject;

import static android.app.Activity.RESULT_OK;
import static com.example.weatherinfophotos.util.CommonUtils.getImageUri;
import static com.example.weatherinfophotos.util.CommonUtils.getRealPathFromURI;

public class HomeFragment extends BaseFragment<HomeFragmentBinding, HomeViewModel> implements LocationHelper.LocationLisetner {

    @Inject
    ViewModelProviderFactory factory;

    private LocationHelper locationHelper;
    private final int REQUEST_COARSE_LOCATION = 3;
    private final int REQUEST_CAMERA = 4;
    private NavController navController;
    private String lat, lon;

    @Override
    public int getLayoutId() {
        return R.layout.home_fragment;
    }

    @Override
    public HomeViewModel getViewModel() {
        return ViewModelProviders.of(this, factory).get(HomeViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showToolbarAndBottomNav();
        locationHelper = new LocationHelper(getActivity(), this);
        // isStoragePermissionGranted();
        navController = Navigation.findNavController(getActivity(), R.id.container_fragment);
        getViewDataBinding().takePhoto.setOnClickListener(v -> {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                    ||
                    ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    ||
                    ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA);
            } else takePhoto(REQUEST_CAMERA);
        });
    }

    private void takePhoto(int cameraRequest) {
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            this.startActivityForResult(takePictureIntent, cameraRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        locationHelper.init();
    }


    private void showToolbarAndBottomNav() {
        if (getActivity() != null) {
            Toolbar mToolbar = getActivity().findViewById(R.id.toolbar);
            mToolbar.findViewById(R.id.share).setVisibility(View.GONE);
            mToolbar.findViewById(R.id.back).setVisibility(View.GONE);
            mToolbar.setVisibility(View.VISIBLE);
            BottomNavigationView bottomNavigationView = getActivity().findViewById(R.id.nav_view);
            bottomNavigationView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void getLocation(Location location) {
        if (location != null) {
            locationHelper.stopLocationUpdate(getActivity());
            Log.d("test_lat", location.getLatitude() + "");
            Log.d("test_lon", location.getLongitude() + "");
            lat = location.getLatitude() + "";
            lon = location.getLongitude() + "";
        } else {
        }

    }

    @Override
    public void sendRequestPermission() {
        requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_COARSE_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_COARSE_LOCATION) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationHelper.init();
            } else {
                Toast.makeText(getActivity(), "location permission denied", Toast.LENGTH_LONG).show();
            }
        }
        if (requestCode == REQUEST_CAMERA) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "camera permission granted", Toast.LENGTH_LONG).show();
                takePhoto(REQUEST_CAMERA);
            } else {
                Toast.makeText(getActivity(), "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("onActivityResultLog", "onActivityResult");
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Uri imageUri = data.getData();
                try {

                    String path = null;
                    if (imageUri != null)
                        path = getRealPathFromURI(imageUri, getActivity());
                    else {
                        Bitmap imageBitMap = (Bitmap) data.getExtras().get("data");
                        Uri imageURI = getImageUri(getActivity(), imageBitMap);
                        path = getRealPathFromURI(imageURI, getActivity());
                    }

                    File imgFile = new File(path);
                    if (imgFile.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        getViewDataBinding().imageViewMainCloud.setImageBitmap(myBitmap);
                    }
                    Log.d("CameraImagePath", "_cameraImagePath: " + path);

                    Bundle args = new Bundle();
                    args.putString("img_path", path);
                    args.putString("lon", lon);
                    args.putString("lat", lat);
                    navController.navigate(R.id.action_navigation_home_to_weatherFragment, args);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

}
