package com.example.weatherinfophotos.ui.main;

import com.example.weatherinfophotos.base.BaseViewModel;
import com.example.weatherinfophotos.data.Repository;
import com.example.weatherinfophotos.di.provides.SchedulerProvider;

public class MainViewModel extends BaseViewModel {
    public MainViewModel(Repository repository, SchedulerProvider schedulerProvider) {
        super(repository, schedulerProvider);
    }
}
