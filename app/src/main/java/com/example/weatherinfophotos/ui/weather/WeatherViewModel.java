package com.example.weatherinfophotos.ui.weather;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.weatherinfophotos.base.BaseViewModel;
import com.example.weatherinfophotos.data.Repository;
import com.example.weatherinfophotos.data.db.entity.WeatherImages;
import com.example.weatherinfophotos.data.models.WeatherResponse;
import com.example.weatherinfophotos.di.provides.SchedulerProvider;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;

public class WeatherViewModel extends BaseViewModel {

    private final Repository repository;
    private final SchedulerProvider schedulerProvider;
    private MutableLiveData<WeatherResponse> weatherMutableLiveData;

    public WeatherViewModel(Repository repository, SchedulerProvider schedulerProvider) {
        super(repository, schedulerProvider);
        this.repository = repository;
        this.schedulerProvider = schedulerProvider;
        this.weatherMutableLiveData = new MutableLiveData<>();

    }

    void getWeatherData(String lat, String lon) {
        setIsLoading(true);

        getCompositeDisposable().add(repository.getWeather(lat, lon).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(schedulerProvider.io())
                .subscribeWith(new DisposableSingleObserver<WeatherResponse>() {

                    @Override
                    public void onSuccess(WeatherResponse weatherResponse) {
                        setIsLoading(false);
                        if (weatherResponse.getCod() == 200)
                            weatherMutableLiveData.setValue(weatherResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        setIsLoading(false);

                    }
                }));
    }

    void insertImage(WeatherImages weatherImages) {
        repository.insertImage(weatherImages);
    }

    public MutableLiveData<WeatherResponse> getWeatherMutableLiveData() {
        return weatherMutableLiveData;
    }

}
