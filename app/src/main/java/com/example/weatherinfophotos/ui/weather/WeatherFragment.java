package com.example.weatherinfophotos.ui.weather;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;

import com.example.weatherinfophotos.R;
import com.example.weatherinfophotos.base.BaseFragment;
import com.example.weatherinfophotos.data.db.entity.WeatherImages;
import com.example.weatherinfophotos.databinding.WeatherFragmentBinding;
import com.example.weatherinfophotos.di.builder.ViewModelProviderFactory;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

import javax.inject.Inject;

public class WeatherFragment extends BaseFragment<WeatherFragmentBinding, WeatherViewModel> {

    @Inject
    ViewModelProviderFactory factory;
    private Handler handler;
    private Runnable r;
    private File f;
    private Toolbar mToolbar;


    @Override
    public int getLayoutId() {
        return R.layout.weather_fragment;
    }

    @Override
    public WeatherViewModel getViewModel() {
        return ViewModelProviders.of(this, factory).get(WeatherViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showToolbarAndBottomNav();

        Bundle args = getArguments();
        if (args != null) {
            String img_path = args.getString("img_path");
            String lat = args.getString("lat");
            String lon = args.getString("lon");
            getViewModel().getWeatherData(lat, lon);
            if (img_path != null) {
                File imgFile = new File(img_path);
                if (imgFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    getViewDataBinding().imageViewPhotoTaken.setImageBitmap(myBitmap);
                }
            }
            observeWeatherData();

            handler = new Handler();
            r = this::capture_Pic;
            handler.postDelayed(r, 2000);


        }
    }

    private void observeWeatherData() {
        getViewModel().getWeatherMutableLiveData().observe(getViewLifecycleOwner(), weatherResponse -> {
            Log.d("weather_res", weatherResponse.getName());
            getViewDataBinding().setWeatherData(weatherResponse);

        });
    }

    private void showToolbarAndBottomNav() {
        if (getActivity() != null) {
            mToolbar = getActivity().findViewById(R.id.toolbar);
            mToolbar.findViewById(R.id.share).setVisibility(View.VISIBLE);
            ImageView back = mToolbar.findViewById(R.id.back);
            back.setVisibility(View.VISIBLE);
            back.setOnClickListener(v -> getActivity().onBackPressed());
            mToolbar.setVisibility(View.VISIBLE);
            BottomNavigationView bottomNavigationView = getActivity().findViewById(R.id.nav_view);
            bottomNavigationView.setVisibility(View.GONE);
        }
    }

    private void capture_Pic() {

        getViewDataBinding().rootView.setDrawingCacheEnabled(true);
        getViewDataBinding().rootView.buildDrawingCache();
        Bitmap bm = getViewDataBinding().rootView.getDrawingCache();
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/*");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        Random random = new Random();
        int x = random.nextInt(100);
        f = new File(Environment.getExternalStorageDirectory() + File.separator + "image" + x + ".jpeg");
        Log.d("file_Sc", f.getPath());

        new Thread() {
            @Override
            public void run() {
                getViewModel().insertImage(new WeatherImages(f.getPath()));
            }
        }.start();
        mToolbar.findViewById(R.id.share).setOnClickListener(v -> {
            Log.d("files_path", f.getPath());
            share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
            //  share.setPackage("com.twitter.android");
            startActivity(Intent.createChooser(share, "Share via"));
        });

        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        handler.removeCallbacks(r);
    }

}
