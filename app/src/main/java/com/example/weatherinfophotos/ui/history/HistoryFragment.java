package com.example.weatherinfophotos.ui.history;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;

import com.example.weatherinfophotos.R;
import com.example.weatherinfophotos.base.BaseFragment;
import com.example.weatherinfophotos.databinding.HistoryFragmentBinding;
import com.example.weatherinfophotos.di.builder.ViewModelProviderFactory;

import javax.inject.Inject;

public class HistoryFragment extends BaseFragment<HistoryFragmentBinding, HistoryViewModel> {

    @Inject
    ViewModelProviderFactory factory;
    private NavController navController;
    @Override
    public int getLayoutId() {
        return R.layout.history_fragment;
    }

    @Override
    public HistoryViewModel getViewModel() {
        return ViewModelProviders.of(this, factory).get(HistoryViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(getActivity(), R.id.container_fragment);

        observeDataBase();
    }

    private void observeDataBase() {
        getViewModel().getListImagesMutableLiveData().observe(getViewLifecycleOwner(), weatherImages -> {
            Log.d("test_db", weatherImages.size() + "");
            ImagesAdapter imagesAdapter = new ImagesAdapter(getActivity(), weatherImages);
            getViewDataBinding().imageRV.setAdapter(imagesAdapter);
            getViewDataBinding().imageRV.setLayoutManager(new GridLayoutManager(getActivity(), 3));
            imagesAdapter.notifyDataSetChanged();
        });
    }

}
