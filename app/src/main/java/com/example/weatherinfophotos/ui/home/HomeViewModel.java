package com.example.weatherinfophotos.ui.home;

import com.example.weatherinfophotos.base.BaseViewModel;
import com.example.weatherinfophotos.data.Repository;
import com.example.weatherinfophotos.di.provides.SchedulerProvider;

public class HomeViewModel extends BaseViewModel {

    Repository repository;
    public HomeViewModel(Repository repository, SchedulerProvider schedulerProvider) {
        super(repository, schedulerProvider);
        this.repository = repository;
    }

}
